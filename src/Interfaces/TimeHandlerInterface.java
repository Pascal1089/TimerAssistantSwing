package Interfaces;

import java.util.Date;

/**
 * Created by Pascal on 25.01.2018.
 */
public interface TimeHandlerInterface {

    public long getTimeInMilliseconds();

    public void setNewEndTime(long newEndTime);

    public Date getShutdownTimeAsDate();

}
