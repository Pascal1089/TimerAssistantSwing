/**
 * Created by Pascal on 21.12.2017.
 */
public class OSDeterminer {
    public static enum osType {
        WIN,
        MAC,
        LINUX,
        NOT_DETERMIND
    }

    public osType determineOS(){
        String oSName = System.getProperty("os.name").toLowerCase();
        if(oSName.contains("win")){
            return osType.WIN;
        }else if(oSName.contains("mac")){
            return osType.MAC;
        }else if(oSName.contains("nix")){
            return osType.LINUX;
        }else{
            return osType.NOT_DETERMIND;
        }
    }
}
