import Interfaces.TimeHandlerInterface;

import java.util.Date;

/**
 * Created by Pascal on 25.01.2018.
 */
public class TimeHandler implements TimeHandlerInterface {

    private Date shutdownTime;

    @Override
    public long getTimeInMilliseconds() {
        return shutdownTime.getTime();
    }

    @Override
    public void setNewEndTime(long newEndTime) {
        shutdownTime = new Date(newEndTime + System.currentTimeMillis());
    }

    @Override
    public Date getShutdownTimeAsDate() {
        return shutdownTime;
    }
}
