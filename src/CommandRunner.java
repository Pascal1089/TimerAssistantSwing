import java.io.IOException;

/**
 * Created by Pascal on 02.10.2017.
 */
public class CommandRunner {
    private String[] windowsShutdownCommandString = {"shutdown","/s"};
    private String[] macShutdownCommandString = {"sudo","shutdown"};
    private String[] linuxShutdownCommandString = {"halt"};
    private String[] shutdownCommand;

    public CommandRunner(OSDeterminer.osType OSName){
        switch (OSName){
            case WIN:
                shutdownCommand = windowsShutdownCommandString;
                break;
            case MAC:
                shutdownCommand = macShutdownCommandString;
                break;
            case LINUX:
                shutdownCommand = linuxShutdownCommandString;
                break;
            default:
                break;
        }
    }

    public void sendShutdownCommand(String password){
        String[] command = buildCommandString(password);
        try{
            Runtime.getRuntime().exec(command);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String[] buildCommandString(String password){
        if(!password.equals(""))
            return new String[]{shutdownCommand[0], password, shutdownCommand[1]};
        return shutdownCommand;
    }
}
