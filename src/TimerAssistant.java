import CustomizedClasse.ButtonWithIcon;
import CustomizedClasse.CustomizedButton;
import CustomizedClasse.FrameDragListener;

import javax.swing.*;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.MenuItem;
import java.awt.PopupMenu;
import java.awt.SystemTray;
import java.awt.Toolkit;
import java.awt.TrayIcon;
import java.util.TimerTask;
import java.util.Timer;

import static javax.swing.JOptionPane.showMessageDialog;

/**
 * Created by Pascal Walter on 20.09.2017.
 */
public class TimerAssistant extends JFrame {

    private static int MAIN_FRAME_WIDTH = 300;
    private static int MAIN_FRAME_HEIGHT = 100;
    private TimeHandler statusSaver;
    private Timer countDown;
    private Timer checkForSleepModeTimer;

    private SystemTray tray;

    private TrayIcon trayIcon;
    private JSpinner minutesSpinner;

    private JSpinner hoursSpinner;
    private DialogFactory dialogFactory;
    private CommandRunner commandRunner;

    private JButton startTimerButton;
    private JButton abortTimerButton;
    private JButton extendTimerButton;

    private static String ICON_PATH = "icon.jpg";
    private static Color BACKGROUND_COLOR = new Color(81,80,80);

    private static int TIME_STAMP_DELAY_IN_MS = 1500;

    private OSDeterminer.osType osType;

    private boolean validTimerExists = false;


    public TimerAssistant() {
        buildGUI();
        dialogFactory = new DialogFactory(this);
        statusSaver = new TimeHandler();
        OSDeterminer osDeterminer = new OSDeterminer();
        osType = osDeterminer.determineOS();
        commandRunner = new CommandRunner(osType);
    }


    private void buildGUI() {
        this.setUndecorated(true);
        this.getContentPane().setBackground(BACKGROUND_COLOR);

        JPanel buttonPanel = createButtonPanel();
        JPanel minutesAndHoursPanel = createSpinnerPanel();

        buttonPanel.setBackground(BACKGROUND_COLOR);
        minutesAndHoursPanel.setBackground(BACKGROUND_COLOR);

        add(buttonPanel, BorderLayout.NORTH);
        add(minutesAndHoursPanel, BorderLayout.SOUTH);
        setSize(MAIN_FRAME_WIDTH, MAIN_FRAME_HEIGHT);

        Dimension screenDimension = Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation(screenDimension.width / 2 - this.getSize().width/2,
                screenDimension.height / 2 - this.getSize().height /2);
        this.setVisible(true);

        FrameDragListener dragListener = new FrameDragListener(this);

        this.addMouseListener(dragListener);
        this.addMouseMotionListener(dragListener);
        ImageIcon imageIcon = new ImageIcon(ICON_PATH);
        this.setIconImage(imageIcon.getImage());
    }

    private JPanel createButtonPanel(){
        JPanel buttonPanel = new JPanel();
        startTimerButton = new CustomizedButton("Start Timer");
        startTimerButton.addActionListener(e -> startTimerButtonPushed());

        abortTimerButton = new CustomizedButton("Abort Timer");
        abortTimerButton.setEnabled(false);
        abortTimerButton.addActionListener(e -> abortTimerButtonPushed());

        extendTimerButton = new CustomizedButton("Extend Timer");
        extendTimerButton.setEnabled(false);
        extendTimerButton.addActionListener(e -> extendTimer());
        ImageIcon closeIcon = new ImageIcon(getClass().getResource("close.png"));
        JButton closeProgram = new ButtonWithIcon(closeIcon);
        closeProgram.setSize(248,128);
        closeProgram.addActionListener(e -> closeProgram());
        buttonPanel.add(startTimerButton);
        buttonPanel.add(abortTimerButton);
        buttonPanel.add(extendTimerButton);
        if(SystemTray.isSupported()) {
            ButtonWithIcon sendToTrayButton = new ButtonWithIcon(new ImageIcon(getClass().getResource("minimize.png")));
            sendToTrayButton.addActionListener(e -> sendToTray());
            buttonPanel.add(sendToTrayButton);
        }
        buttonPanel.add(closeProgram);
        buttonPanel.setVisible(true);
        return buttonPanel;
    }

    private JPanel createSpinnerPanel(){
        JPanel minutesAndHoursPanel = new JPanel();
        JLabel hoursSpinnerLabel = new JLabel("Hours:");
        JLabel minutesSpinnerLabel = new JLabel("Minutes");

        Dimension spinnerDimension = new Dimension(50,25);

        hoursSpinner = new JSpinner();
        SpinnerNumberModel hoursModel = new SpinnerNumberModel();
        hoursModel.setMaximum(3);
        hoursModel.setMinimum(0);
        hoursModel.setStepSize(1);
        hoursModel.setValue(0);
        hoursSpinner.setModel(hoursModel);
        hoursSpinner.setSize(spinnerDimension);
        hoursSpinner.setPreferredSize(spinnerDimension);
        hoursSpinner.setMinimumSize(spinnerDimension);

        minutesSpinner = new JSpinner();
        SpinnerNumberModel minutesModel = new SpinnerNumberModel();
        minutesModel.setMaximum(59);
        minutesModel.setMinimum(1);
        minutesModel.setValue(1);
        minutesModel.setStepSize(1);
        minutesSpinner.setModel(minutesModel);
        minutesSpinner.setSize(spinnerDimension);
        minutesSpinner.setPreferredSize(spinnerDimension);
        minutesSpinner.setMinimumSize(spinnerDimension);

        minutesAndHoursPanel.add(hoursSpinnerLabel);
        minutesAndHoursPanel.add(hoursSpinner);
        minutesAndHoursPanel.add(minutesSpinnerLabel);
        minutesAndHoursPanel.add(minutesSpinner);
        minutesAndHoursPanel.setVisible(true);

        return minutesAndHoursPanel;
    }

    private void startTimerButtonPushed(){
        saveStatus();
        startTimer();
    }

    private void startTimer() {
        validTimerExists = true;
        sendToTray();
        toggleButtons();
        setTimerTask();
        checkIfSystemWasInSleepMode();
    }

    private void setTimerTask(){
        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                commandRunner.sendShutdownCommand("");
            }
        };
        countDown = new Timer();
        countDown.schedule(task,statusSaver.getShutdownTimeAsDate());
    }

    private void abortTimerButtonPushed(){
        int answer = dialogFactory.buildDialog(DialogFactory.DialogSort.ABORT_TIMER);
        if(answer == 0){
            abortTimer();
        }
    }

    private void abortTimer() {
        validTimerExists = false;
        toggleButtons();
        countDown.cancel();
        checkForSleepModeTimer.cancel();
    }

    private void toggleButtons(){
        extendTimerButton.setEnabled(!extendTimerButton.isEnabled());
        startTimerButton.setEnabled(!startTimerButton.isEnabled());
        abortTimerButton.setEnabled(!abortTimerButton.isEnabled());
    }

    private void extendTimer() {
        abortTimer();
        int answer = dialogFactory.buildDialog(DialogFactory.DialogSort.EXTEND_TIMER);
        if(answer == 0){
            startTimerButtonPushed();
        }
    }

    public static void main(String[] args) {
        new TimerAssistant();
    }

    private void closeProgram(){
        int answer;
        if(validTimerExists){
            answer = dialogFactory.buildDialog(DialogFactory.DialogSort.CLOSE_DIALOG_TIMER_ACTIVE);
        }else{
            answer = dialogFactory.buildDialog(DialogFactory.DialogSort.CLOSE_DIALOG_TIMER_INACTIVE);
        }
        if(answer == 0) {
            validTimerExists = false;
            System.exit(0);
        }
    }

    private int[] getInputValues(){
        return new int[]{(Integer) hoursSpinner.getValue(),(Integer) minutesSpinner.getValue()};
    }

    private void saveStatus(){
        int[] values = getInputValues();
        statusSaver.setNewEndTime(hoursAndMinutesToMillis(values[0], values[1]));
            showMessageDialog(this, "Device gets shut down in "+
            "\n" + values[0] + " hours and " + values[1] + " minutes.");
    }

    private long hoursAndMinutesToMillis(int hours, int minutes){
        return (long) ((60 * 60 * hours + 60 * minutes) * 1000);
    }

    private void sendToTray(){
        int answer = dialogFactory.buildDialog(DialogFactory.DialogSort.SEND_TO_TRAY);
        if(answer == 0){
            tray = SystemTray.getSystemTray();
            PopupMenu trayMenu = new PopupMenu();
            MenuItem showGUICloseTray = new MenuItem("Open program");
            showGUICloseTray.addActionListener(e -> showGUICloseTray());

            MenuItem abortTimer = new MenuItem("Close");
            abortTimer.addActionListener(e -> closeProgram());

            trayMenu.add(showGUICloseTray);
            trayMenu.add(abortTimer);

            trayIcon = new TrayIcon(
                    new ImageIcon(getClass().getResource(ICON_PATH), "Timer Assistant").getImage(),
                    "Timer Assistant" ,trayMenu
            );
            trayIcon.setPopupMenu(trayMenu);
            trayIcon.setImageAutoSize(true);
            try{
                tray.add(trayIcon);
            }catch (Exception e){
                showMessageDialog(this, "Tray not supported!");
            }
            this.setVisible(false);
        }
    }

    private void closeTray(){
        tray.remove(trayIcon);
    }

    private void showGUICloseTray(){
        this.setVisible(true);
        closeTray();
    }

    private void checkIfSystemWasInSleepMode(){
        checkForSleepModeTimer = new Timer();
        TimerTask task = new TimerTask() {
            private long lastTime = System.currentTimeMillis();
            private long shutdownTime = statusSaver.getTimeInMilliseconds();
            @Override
            public void run() {
                long currentTime = System.currentTimeMillis();
                if(currentTime > shutdownTime
                        || currentTime - lastTime > TIME_STAMP_DELAY_IN_MS){
                    int answer = dialogFactory.buildDialog(DialogFactory.DialogSort.SLEEP_MODE_DETECTED);
                    if(answer == 0){
                        abortTimer();
                    }else{
                        checkForSleepModeTimer.cancel();
                        lastTime = currentTime;
                    }
                }else{
                    lastTime = currentTime;
                }
            }
        };
        checkForSleepModeTimer.schedule(task,1000, 1000);
    }
}
