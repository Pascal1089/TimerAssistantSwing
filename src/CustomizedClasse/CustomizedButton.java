package CustomizedClasse;

import javax.swing.JButton;
import java.awt.*;

/**
 * Created by Pascal on 13.10.2017.
 */
public class CustomizedButton extends JButton {
    private Color backgroundColor = new Color(18,17,17);
    private Font buttonFont = new Font("Tahoma",Font.PLAIN, 12);
    private Color foreGroundColor = new Color(255,255,255);
    public CustomizedButton(String title){
        super(title);
        super.setBackground(backgroundColor);
        super.setFont(buttonFont);
        super.setFocusPainted(false);
        super.setForeground(foreGroundColor);
        super.setBorderPainted(false);
        super.setBorder(null);
        super.setMargin(new Insets(100,100,100,100));
    }
}
