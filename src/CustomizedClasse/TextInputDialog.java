package CustomizedClasse;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import java.awt.event.*;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

/**
 * Created by Pascal on 21.12.2017.
 */
public class TextInputDialog extends JDialog implements ActionListener, PropertyChangeListener {

    private String message;
    private String ENTER_BTN_TEXT = "Enter";
    private String CLOSE_BTN_TEXT = "Close";
    private JOptionPane optionPane;
    private JTextField textField;
    public TextInputDialog(JFrame parentFrame, String message, String title){
        super(parentFrame);
        this.message = message;
        setTitle(title);
        Object[] options = {ENTER_BTN_TEXT, CLOSE_BTN_TEXT};
        Object[] messageArray = {message, textField};
        optionPane = new JOptionPane(messageArray,
                JOptionPane.QUESTION_MESSAGE,
                JOptionPane.YES_NO_OPTION,
                null,
                options,
                options[0]
        );
        setContentPane(optionPane);

        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent we) {
                /*
                 * Instead of directly closing the window,
                 * we're going to change the JOptionPane's
                 * value property.
                 */
                optionPane.setValue(new Integer(
                        JOptionPane.CLOSED_OPTION));
            }
        });

        //Ensure the text field always gets the first focus.

        //Register an event handler that puts the text into the option pane.

        //Register an event handler that reacts to option pane state changes.
        optionPane.addPropertyChangeListener(this);

        setVisible(true);
    }


    @Override
    public void actionPerformed(ActionEvent e) {

    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {

    }
}
