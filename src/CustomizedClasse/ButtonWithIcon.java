package CustomizedClasse;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Pascal on 21.10.2017.
 */
public class ButtonWithIcon extends JButton {
    public ButtonWithIcon(ImageIcon icon){
        super(icon);
        setSize(18,18);
        setBorder(null);
    }
}
