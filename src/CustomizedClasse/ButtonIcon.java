package CustomizedClasse;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Pascal on 05.12.2017.
 */
public class ButtonIcon implements Icon {

    private int iconWidth;
    private int iconHeight;

    @Override
    public void paintIcon(Component c, Graphics g, int x, int y) {

    }

    @Override
    public int getIconWidth() {
        return this.iconWidth;
    }

    @Override
    public int getIconHeight() {
        return this.iconHeight;
    }
}
