package CustomizedClasse;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * Created by Pascal on 14.10.2017.
 */
public class FrameDragListener extends MouseAdapter{
    private final JFrame frame;
    private Point mousePosition = null;
    public FrameDragListener(JFrame frame){
        this.frame = frame;
    }

    public void mousePressed(MouseEvent e){
        mousePosition = e.getPoint();
    }

    public void mouseReleased(MouseEvent e){
        mousePosition = null;
    }

    public void mouseDragged(MouseEvent e){
        Point currentPosition = e.getLocationOnScreen();
        frame.setLocation(currentPosition.x - mousePosition.x, currentPosition.y - mousePosition.y);
    }
}
