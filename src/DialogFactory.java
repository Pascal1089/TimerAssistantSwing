import CustomizedClasse.TextInputDialog;

import javax.swing.*;

/**
 * Created by Pascal on 25.09.2017.
 */
public class DialogFactory {

    public enum DialogSort {
        CLOSE_DIALOG_TIMER_INACTIVE,
        CLOSE_DIALOG_TIMER_ACTIVE,
        ABORT_TIMER,
        EXTEND_TIMER,
        SEND_TO_TRAY,
        SLEEP_MODE_DETECTED
    }

    private JFrame parentFrame;

    public DialogFactory(JFrame parentFrame){
        this.parentFrame = parentFrame;
    }

    public int buildDialog(DialogSort sort){
        String dialogTitle;
        String dialogMessage;

        switch (sort){
            case CLOSE_DIALOG_TIMER_INACTIVE:
                dialogTitle = "Close the Timer Assistant";
                dialogMessage = "Do you want to close the timer?";
                break;
            case CLOSE_DIALOG_TIMER_ACTIVE:
                dialogTitle = "Close the Timer Assisstant";
                dialogMessage = "Do you want to close the timer and end the timer?";
                break;
            case SEND_TO_TRAY:
                dialogTitle = "Send to tray";
                dialogMessage = "Program is send to tray";
                break;
            case ABORT_TIMER:
                dialogTitle = "Abort Timer";
                dialogMessage = "Do you want to abort the timer?";
                break;
            case EXTEND_TIMER:
                dialogTitle = "Extend timer";
                dialogMessage = "Do you want to extend the timer?";
                break;
            case SLEEP_MODE_DETECTED:
                dialogTitle = "Sleep mode detected!";
                dialogMessage = "Do you want abort the timer?";
                break;
            default:
                dialogTitle = "Default";
                dialogMessage = "FOO BAR";
                break;
        }

        return JOptionPane.showConfirmDialog(
                parentFrame,
                dialogMessage,
                dialogTitle,
                JOptionPane.YES_NO_OPTION
                );
    }

    public TextInputDialog buildPasswordInputDialog(){

        return new TextInputDialog(parentFrame, "Enter sudo password", "Confirm Timer start");
    }
}
